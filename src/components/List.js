import React, { useEffect, useState } from 'react'
import '../css/List.css'

export default function List() {
    //Les useStates sont des variables d'état, elles ne sont pas supprimées tant que la page n'est pas rechargée 
    const [comments, setComments] = useState([]);
    const [valueInput, setValueInput] = useState([]);

    useEffect(() => {
        // Lorsque la page List.js est chargé, récupère les données du localStorage
        const storedComments = JSON.parse(localStorage.getItem('comments')) || []; //Converti la chaîne json stocké dans le localStorage en objet javascript et le stock dans une variable
        setComments(storedComments); //Stock les données dans le useState "comments"
    }, []);

    const ajouterTache = () => {
        if (valueInput.trim() === '') { //Si la valeur de l'input (en retirant tous les espaces, tabulations, retour à la ligne..), est vide
            alert('Veuillez saisir un commentaire non vide.'); //Créer une alerte
            return; //Sort de la fonction
        }

        const updatedComments = [...comments, valueInput];//Ecris le nouveau commentaire dans une variable "updateComments" à la fin de la liste des commentaires déja présent dans le useState "comments"
        localStorage.setItem('comments', JSON.stringify(updatedComments));//Stock dans le localStorage la variable "updatedComments" à la place de l'ancienne valeur présente pour la clé "comments"
        setComments(updatedComments);//Met à jour la variable d'état "comments" avec la valeur de la variable updatedComments
        setValueInput('');//Permet de vider la valeur de l'input
    };

    const supprimerTache = (index) => {
        const updatedComments = comments.filter((comment, i) => i !== index); //Permet de mettre à jour le tableau de commentaire en excluant le commentaire ayant pour key "index"
        console.log(updatedComments)
        localStorage.setItem('comments', JSON.stringify(updatedComments));//Met à jour le localStorage ayant pour clé "comments"
        setComments(updatedComments); //Met à jour la liste des commentaires dans le useState
    };

    const handleKeyDown = (event) => { //Permet d'actionner la fonction "ajouterTache" lorsque la touche entrer est appuyée et que le focus est sur l'input
        if (event.key === 'Enter') {
            ajouterTache();
        }
    };

    return (
        <div className='container'>
            <div className='container__input'>
                <input className='inputText' onKeyDown={handleKeyDown} min="1" type='text' placeholder='Ajoute une tâche à ta liste...' value={valueInput} onChange={(e) => setValueInput(e.target.value)}></input>
                <button className='buttonAdd' onClick={ajouterTache}>Add</button>
            </div>
            <div className='groupComments'>
                {comments.map((comment, index) => ( //Permet de boucler sur chaque commentaire présent dans le useState "comments", en le liant avec un index
                    <div className='divComment' key={index}>
                        <p className='pComment'>{comment}</p>
                        <p className='poubelleComment' onClick={() => supprimerTache(index)}>🗑️</p>
                    </div>
                ))}
            </div>
        </div>
    )
}
